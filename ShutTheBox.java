public class ShutTheBox {
	
	//Main Method
	public static void main(String[] args){
		
		//Welcome message
		System.out.println("Welcome to Shut The Box!");
		
		//Creating board object
		Board board = new Board();
		
		
		//Executing the game
		boolean gameOver = false;
		
		//As long as boolean game over is false, the game continues calling the playATurn method
		while(!gameOver){
			System.out.println("Player 1's turn");
			System.out.println(board);
			
			if(board.playATurn()) {
				System.out.println("Player 2 wins!");
				gameOver = true;
			} else {
				System.out.println("Player 2's turn");
				System.out.println(board);
				if(board.playATurn()) {
					System.out.println("Player 1 wins!");
					gameOver = true;
				}
			}
		}
	}
}