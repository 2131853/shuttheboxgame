public class Board {

	//Private fields of the board
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	//Constructor
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
		for(int i=0; i<this.closedTiles.length; i++){
			this.closedTiles[i] = false;
		}
		
	}
	
	//toString method that shows which tiles have been flipped on the board
		public String toString(){
			
			String str = " ";
			
		for(int i= 0; i < this.closedTiles.length; i++){
			
			if(this.closedTiles[i]) {
				str = str + (i+1) + " ";
			} else {
				str = str + ("X ");
			}
			} return str;
		}
		
		//playATurn method
		public boolean playATurn(){
			
			//Roll both die and show what has been rolled
			die1.roll();
			die2.roll();
			
			System.out.println(die1 + "and " +die2+ "has been rolled!");
			
			// Checking if tile has been closed or not, to see if game will end or nto
			int sum = die1.getPips() + die2.getPips();
			if(this.closedTiles[sum-1]) {
				System.out.println("Tile " + sum + " is already shut!");
				return true;
			} else {
				closedTiles[sum-1] = true;
				System.out.println("Closing tile: " + (sum));
				return false;
			}
		}
	}
