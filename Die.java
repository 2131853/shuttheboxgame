import java.util.Random;
public class Die {
	
	//private fields
	private int pips;
	private Random r;

	//constructor
	public Die(){
		this.pips = 1;
		this.r = new Random();
	}
	
	//get method for pips
	public int getPips(){
		return this.pips;
	}
	
	//roll method that sets the pip field to a random number from 1-6
	public void roll(){
		this.pips = r.nextInt(6) + 1;
	}
	
	//toString method that returns the pips value of dice
	public String toString(){
		return this.pips + " ";
	}
}